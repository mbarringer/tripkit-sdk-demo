package com.skedgo.tripkit.ui.demo

import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.skedgo.android.common.model.Location
import com.skedgo.android.common.model.Query
import com.skedgo.android.common.model.ScheduledStop
import com.skedgo.android.common.model.TimeTag
import com.skedgo.android.common.model.TimeTag.TIME_TYPE_ARRIVE_BY
import com.skedgo.android.common.model.TimeTag.TIME_TYPE_LEAVE_AFTER
import com.skedgo.tripkit.model.ViewTrip
import com.skedgo.tripkit.ui.demo.databinding.ActivityMapsBinding
import com.skedgo.tripkit.ui.dialog.TripKitDateTimePickerDialogFragment
import com.skedgo.tripkit.ui.locationhelper.LocationHelper
import com.skedgo.tripkit.ui.map.home.TripKitMapFragment
import com.skedgo.tripkit.ui.routeinput.RouteInputView
import com.skedgo.tripkit.ui.search.LocationSearchFragment
import com.skedgo.tripkit.ui.tripresults.TripResultListFragment
import com.skedgo.tripkit.ui.utils.formatString
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.DialogInterface
import android.os.Build
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    // These are used with onRequestPermissionsResult to get the user's permission to use the GPS.
    private var PERMISSION_FOR_START = 1000
    private var PERMISSION_FOR_DEST = 1001

    // We need to keep track of the start and destination locations for use with the routing fragments.
    private var fromLocation: Location? = null
    private var toLocation: Location? = null

    // A TimeTag represents a departure or arrival time. We'll create it for Leave Now by default.
    private var timeTagForQuery: TimeTag = TimeTag.createForLeaveNow()

    // The LocationHelper is a small wrapper class around the FusedLocationProviderClient.
    private lateinit var locationHelper: LocationHelper

    // Finally, we use a custom view to display the Start, Destination, Time and Route buttons.
    private lateinit var routeInputView: RouteInputView

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationHelper = LocationHelper(this)

        val binding : ActivityMapsBinding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        routeInputView = binding.routeInputView

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as TripKitMapFragment
        // When the user clicks on a stop icon in the map, a small info window is displayed. We want to
        // then show a timetable for that specific stop when the info window is clicked.
        mapFragment.setOnInfoWindowClickListener { location ->
            if (location is ScheduledStop) {
                showTimetable(location)
            }
        }

        // The RouteInputView doesn't do anything more than show data. We hook into the clicked listener
        // to provide interaction.
        routeInputView.setOnRouteWidgetClickedListener {widget ->
            when(widget) {
                RouteInputView.OnRouteWidgetClickedListener.Widget.START -> showSearchForStart()
                RouteInputView.OnRouteWidgetClickedListener.Widget.DESTINATION -> showSearchForDestination()
                RouteInputView.OnRouteWidgetClickedListener.Widget.SWAPPED -> startAndDestinationSwapped()
                RouteInputView.OnRouteWidgetClickedListener.Widget.ROUTE ->routeButtonSelected()
                RouteInputView.OnRouteWidgetClickedListener.Widget.TIME -> timeButtonSelected()
            }
        }

        // Finally, tell the fragment to load the map.
        mapFragment.getMapAsync(this)
    }

    // The map is ready, let's focus on Sydney by default.
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val sydney = LatLng(-34.0, 151.0)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        mMap.moveCamera(CameraUpdateFactory.zoomTo(12.0f))
    }

    // The user clicked on the "Start" search field, so we'll load up the LocationSearchFragment to allow
    // them to choose a location.
    private fun showSearchForStart() {
        val locationSetFunction: (Location?) -> Unit = { location: Location? ->
            fromLocation = location
            fromLocation?.let {
                routeInputView.setStartText(it.displayName)
            }
            supportFragmentManager.popBackStackImmediate()
        }

        showSearch(locationSetFunction = locationSetFunction,
                gpsPermission = PERMISSION_FOR_START,
                canAccessGpsFunction = ::currentLocationForStart)
    }

    // The user clicked on the "Destination" search field, so we'll load up the LocationSearchFragment to allow
    // them to choose a location.
    private fun showSearchForDestination() {
        val locationSetFunction: (Location?) -> Unit = { location: Location? ->
            toLocation = location
            toLocation?.let {
                routeInputView.setDestinationText(it.displayName)
            }
            supportFragmentManager.popBackStackImmediate()
        }

        showSearch(locationSetFunction = locationSetFunction,
               gpsPermission = PERMISSION_FOR_DEST,
               canAccessGpsFunction = ::currentLocationForDestination)
    }

    private fun showSearch(locationSetFunction: (Location?) -> Unit,
                           gpsPermission: Int = -1,
                           canAccessGpsFunction: () -> Unit = {}) {
        val fragment = LocationSearchFragment.Builder()
                .withBounds(mMap.projection.visibleRegion.latLngBounds)
                .near(mMap.cameraPosition.target)
                .withHint(getString(R.string.search))
                .allowCurrentLocation(true)
                // We could also show a "Choose on Map" option, but that isn't currently supported by the demo.
                //.allowDropPin()
                .build()

        fragment.setOnLocationSelectedListener(locationSetFunction)
        if (gpsPermission > 0) {
            fragment.setOnCurrentLocationSelectedListener {
                if (canAccessGps()) {
                    canAccessGpsFunction()
                } else {
                    requestPermission(gpsPermission)
                }
            }
        }
        supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    // The user clicked on the two arrows, so swap the start and destination locations.
    private fun startAndDestinationSwapped() {
        fromLocation = toLocation.also { toLocation = fromLocation }
    }

    // They'd like to change the time. The TripKitDateTimePickerDialogFragment just needs a start and destination
    // location and a time tag.
    private fun timeButtonSelected() {
        var fragment = TripKitDateTimePickerDialogFragment
                .Builder()
                .withLocations(toLocation, fromLocation)
                .withTimeTag(timeTagForQuery)
                .build()
        fragment.setOnTimeSelectedListener { tag ->
            this.timeTagForQuery = tag

            // Show the correct time on the button when they've changed the time
            val timeZone: String? = when (tag.type) {
                TIME_TYPE_LEAVE_AFTER -> { toLocation?.timeZone }
                TIME_TYPE_ARRIVE_BY -> { fromLocation?.timeZone }
                else -> null
            }
            routeInputView.setTimeButton(tag.formatString(this, timeZone))
        }
        fragment.show(supportFragmentManager, "timePicker")
    }

    private fun routeButtonSelected() {
        if (fromLocation == null || toLocation == null) {
            return
        }

        // This is ultimately what we're here for - the route query. There are a lot of options,
        // so refer to the Query documentation specifically.
        val query = Query().apply {
            fromLocation = this@MapsActivity.fromLocation
            toLocation = this@MapsActivity.toLocation
            cyclingSpeed = 1
            walkingSpeed = 1
            environmentWeight = 50
            hassleWeight = 25
            budgetWeight = 60
            setTimeTag(timeTagForQuery)
        }

        val fragment = TripResultListFragment.Builder()
                .withQuery(query)
                .build()

        fragment.setOnTripSelectedListener { viewTrip -> tripSelected(viewTrip) }

        // The TripResultFragment shows the start and destination locations. When the user clicks on them,
        // show the LocationSearch so they can change their route query immediately.
        fragment.setOnLocationClickListener(startLocationClicked = {
            showSearch( {location ->
                    val newQuery = fragment.query().clone()
                    newQuery.fromLocation = location
                    fragment.setQuery(newQuery)
                    supportFragmentManager.popBackStackImmediate() })
            }, destinationLocationClicked = {
                    showSearch({ location ->
                        val newQuery = fragment.query().clone()
                        newQuery.toLocation = location
                        fragment.setQuery(newQuery)
                        supportFragmentManager.popBackStackImmediate() })
            })
        supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun tripSelected(viewTrip: ViewTrip) {
        // This is another naming clash - this TripResultFragment is part of the demo, and combines a map with
        // the TripResultPagerFragment which allows users to swipe back and forth between the trip results.
        val tripResultFragment = TripResultFragment.newInstance(viewTrip)
        supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, tripResultFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()

    }


    fun showTimetable(location: ScheduledStop) {
        // This demo combines a map and the timetable into it's own fragment, but you could use either/or if you want.
        val timetablesFragment = TimetableFragment.newInstance(location)
        supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, timetablesFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    // When the user has chosen "Current Location" we initially show "Current Location" in the field,
    // and when the current location has been found, we show the address.
    private fun currentLocationForStart() {
        routeInputView.setStartText(getString(R.string.current_location))
        locationHelper.getCurrentLocation({
            fromLocation = it
            routeInputView.setStartText(it.address)
        }, { // Error handler
            Toast.makeText(this@MapsActivity, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun currentLocationForDestination() {
        routeInputView.setDestinationText(getString(R.string.current_location))
        locationHelper.getCurrentLocation({
            toLocation = it
            routeInputView.setDestinationText(it.address)
        }, {// Error Handler
            Toast.makeText(this@MapsActivity, it, Toast.LENGTH_SHORT).show()
        })
    }

    // The following functions deal with getting permission to use the GPS. We use two permission codes to know
    // if we should go and get the location for the start or destination after permission is granted.

    private fun canAccessGps(): Boolean =
            ContextCompat.checkSelfPermission(applicationContext, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

    private fun requestPermission(code: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), code)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if ((requestCode == PERMISSION_FOR_START || requestCode == PERMISSION_FOR_DEST) && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestCode == PERMISSION_FOR_START) {
                    currentLocationForStart()
                } else {
                    currentLocationForDestination()
                }

            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                    showPermissionRationale(requestCode)
                }
            }
        }
    }

    fun showPermissionRationale(requestCode: Int) {
        AlertDialog.Builder(this).setMessage("This example needs GPS access")
                .setPositiveButton("OK") { dialog: DialogInterface, which: Int ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermission(requestCode)
                    }
                }
                .create().show();
    }
}
