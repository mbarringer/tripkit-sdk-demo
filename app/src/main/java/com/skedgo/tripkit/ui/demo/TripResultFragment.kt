package com.skedgo.tripkit.ui.demo


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.skedgo.tripkit.model.ViewTrip
import com.skedgo.tripkit.ui.tripresult.TripResultMapFragment
import com.skedgo.tripkit.ui.tripresult.TripResultPagerFragment
import com.skedgo.tripkit.ui.tripresult.TripSegmentListFragment

private const val ARG_TRIP = "trip"

class TripResultFragment : Fragment() {
    private var trip: ViewTrip? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            trip = it.getParcelable(ARG_TRIP)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trip_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (trip == null) return

        val mapFragment = TripResultMapFragment.Builder()
                                                    .withTripGroupId(trip!!.tripGroupUUID)
                                                    .build()

        val pagerFragment = TripResultPagerFragment.Builder()
                // We'll show some custom buttons here. They're later handled in the OnTripButtonClickListener.
                .withTripButton("go",  R.layout.go_button)
                .withTripButton("favorite", R.layout.bookmark_button)
                // We optionally hook the map fragment up to the trip results
                .withMapFragment(mapFragment)
                .withViewTrip(trip!!)
                .build()

        pagerFragment.setOnTripKitButtonClickListener { id, tripGroup ->
            if (id == "go") {
                Toast.makeText(context!!, "Will go to ${tripGroup.uuid()}", Toast.LENGTH_SHORT).show()
            } else if (id == "favorite") {
                Toast.makeText(context!!, "Will add ${tripGroup.uuid()} as a favorite", Toast.LENGTH_SHORT).show()
            }
        }

        childFragmentManager
                .beginTransaction()
                .add(R.id.mapFrameLayout, mapFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()

        childFragmentManager
                .beginTransaction()
                .add(R.id.standardBottomSheet, pagerFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    companion object {
        @JvmStatic
        fun newInstance(trip: ViewTrip) =
                TripResultFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_TRIP, trip)
                    }
                }
    }
}
