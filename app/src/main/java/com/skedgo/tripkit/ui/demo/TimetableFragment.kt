package com.skedgo.tripkit.ui.demo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.skedgo.android.common.model.ScheduledStop
import com.skedgo.tripkit.ui.map.servicestop.ServiceStopMapFragment
import com.skedgo.tripkit.ui.servicedetail.ServiceDetailFragment

private const val ARG_LOCATION = "location"

class TimetableFragment : Fragment() {
    private var location: ScheduledStop? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            location = it.getParcelable(ARG_LOCATION)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_timetable, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // We use a BottomSheet overlayed on a map which shows the service's route.
        val bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById<View>(R.id.standardBottomSheet))
        val mapFragment: ServiceStopMapFragment = childFragmentManager.findFragmentById(R.id.mapFragment) as ServiceStopMapFragment;
        mapFragment.setStop(location)

        var timetableFragment = com.skedgo.tripkit.ui.timetables.TimetableFragment.Builder()
                                        .withStop(location)
                // This demo shows the optional custom button support.
                .withButton("route", R.layout.route_button)
                .withButton("favorite", R.layout.bookmark_button)
                .build()

        // The ServiceStopMapFragment can listen for entry selections and then show the route on the map without
        // any other intervention. You could alternatively do it by hand.
        timetableFragment.addOnTimetableEntrySelectedListener(mapFragment)
        timetableFragment.addOnTimetableEntrySelectedListener { timetableEntry, scheduledStop, minStartTime ->
            // When they've clicked on a particular service, we show the details.
            var detailFragment = ServiceDetailFragment.Builder()
                    .withTimetableEntry(timetableEntry)
                    .withStop(scheduledStop)
                    .build()
            // When they click on a particular stop, we can show it on the map automatically if we hook
            // the fragments together.
            detailFragment.addOnScheduledStopClickListener(mapFragment)
            detailFragment.addOnScheduledStopClickListener {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            childFragmentManager
                    .beginTransaction()
                    .replace(R.id.standardBottomSheet, detailFragment)
                    .addToBackStack(null)
                    .commitAllowingStateLoss()
        }

        timetableFragment.setOnTripKitButtonClickListener { id, scheduledStop ->
            if (id == "route") {
                Toast.makeText(activity, "Will route to ${scheduledStop.displayAddress}", Toast.LENGTH_SHORT).show()
            } else if (id == "favorite") {
                Toast.makeText(activity, "Will favorite ${scheduledStop.nameOrApproximateAddress}", Toast.LENGTH_SHORT).show()
            }
        }
        childFragmentManager
                .beginTransaction()
                .add(R.id.standardBottomSheet, timetableFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    companion object {
        @JvmStatic
        fun newInstance(location: ScheduledStop) =
                TimetableFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_LOCATION, location)
                    }
                }
    }
}
